package tsc.abzalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.general.CommandInitException;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputIndex;

public final class TaskDeleteByIndexCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "delete-task-by-index";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Delete task by index.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new CommandInitException();
        System.out.println("DELETE TASK BY INDEX\n");
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        final boolean areTasksExist = taskService.size(currentUserId) != 0;
        if (areTasksExist) {
            taskService.removeByIndex(currentUserId, inputIndex());
            System.out.println("Task was successfully deleted.\n");
            return;
        }
        System.out.println("Tasks list is empty.\n");
    }

}
