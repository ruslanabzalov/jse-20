package tsc.abzalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.general.CommandInitException;
import tsc.abzalov.tm.model.Project;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputDescription;
import static tsc.abzalov.tm.util.InputUtil.inputName;

public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "create-project";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create project.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new CommandInitException();
        System.out.println("PROJECT CREATION\n");
        @NotNull final String projectName = inputName();
        @NotNull final String projectDescription = inputDescription();
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        serviceLocator.getProjectService().create(new Project(projectName, projectDescription, currentUserId));
        System.out.println("Project was successfully created.\n");
    }

}
