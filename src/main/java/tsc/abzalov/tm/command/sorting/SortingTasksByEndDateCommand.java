package tsc.abzalov.tm.command.sorting;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.exception.general.CommandInitException;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.List;

import static tsc.abzalov.tm.enumeration.CommandType.SORTING_COMMAND;

public final class SortingTasksByEndDateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "sort-tasks-by-end-date";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Sort tasks by end date.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return SORTING_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new CommandInitException();
        System.out.println("ALL TASKS LIST SORTED BY END DATE\n");
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        final boolean areTasksExist = taskService.size(currentUserId) != 0;
        if (areTasksExist) {
            @NotNull final List<Task> tasks = taskService.sortByEndDate(currentUserId);
            for (@NotNull final Task task : tasks)
                System.out.println((taskService.indexOf(currentUserId, task) + 1) + ". " + task);
            System.out.println();
            return;
        }
        System.out.println("Tasks list is empty.\n");
    }

}
