package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IAuthService;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.exception.auth.*;
import tsc.abzalov.tm.model.User;

import static org.apache.commons.lang3.ObjectUtils.anyNotNull;
import static org.apache.commons.lang3.StringUtils.isAnyBlank;
import static tsc.abzalov.tm.util.HashUtil.hash;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String currentUserId;

    @Nullable
    private String currentUserLogin;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void register(
            @NotNull final String login, @NotNull final String password,
            @NotNull final String firstName, @Nullable final String lastName,
            @NotNull final String email
    ) throws Exception {
        if (anyNotNull(userService.findByLogin(login), userService.findByEmail(email)))
            throw new UserAlreadyExistsException(login, email);
        @NotNull final User newUser = new User(login, password, firstName, lastName, email);
        currentUserId = newUser.getId();
        currentUserLogin = login;
    }


    @Override
    public void login(@NotNull final String login, @NotNull final String password) throws Exception {
        if (isAnyBlank(login, password)) throw new IncorrectCredentialsException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new UserIsNotExistException(login);
        if (user.getHashedPassword().equals(hash(password))) {
            currentUserId = user.getId();
            currentUserLogin = user.getLogin();
            return;
        }
        throw new AccessDeniedException();
    }

    @Override
    public void logoff() throws Exception {
        if (currentUserId == null || currentUserLogin == null) throw new SessionIsInactiveException();
        currentUserId = null;
        currentUserLogin = null;
    }


    @Override
    public boolean isSessionInactive() {
        return currentUserId == null || currentUserLogin == null;
    }

    @Override
    @NotNull
    public String getCurrentUserLogin() throws Exception {
        if (currentUserId == null || currentUserLogin == null) throw new SessionIsInactiveException();
        return currentUserLogin;
    }

    @Override
    @NotNull
    public String getCurrentUserId() throws Exception {
        if (currentUserId == null || currentUserLogin == null) throw new SessionIsInactiveException();
        return currentUserId;
    }

}
