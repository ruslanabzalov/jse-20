package tsc.abzalov.tm.command.interaction;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.general.CommandInitException;
import tsc.abzalov.tm.util.InputUtil;

import static tsc.abzalov.tm.enumeration.CommandType.INTERACTION_COMMAND;

public final class CommandDeleteProjectTask extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "delete-project-task";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Delete project task.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return INTERACTION_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new CommandInitException();
        System.out.println("DELETE TASK FROM PROJECT\n");
        @NotNull final IProjectTaskService projectTasksService = serviceLocator.getProjectTaskService();
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (projectTasksService.hasData(currentUserId)) {
            System.out.println("Task");
            @NotNull final String taskId = InputUtil.inputId();
            System.out.println();
            if (projectTasksService.findTaskById(currentUserId, taskId) == null) {
                System.out.println("Task was not found.\n");
                return;
            }
            projectTasksService.deleteProjectTaskById(currentUserId, taskId);
            System.out.println("Task was deleted from the project.\n");
            return;
        }
        System.out.println("One of the lists is empty!");
    }

}
