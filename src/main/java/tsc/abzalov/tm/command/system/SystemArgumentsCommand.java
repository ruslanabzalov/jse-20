package tsc.abzalov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.general.CommandInitException;

import static tsc.abzalov.tm.enumeration.CommandType.SYSTEM_COMMAND;

public final class SystemArgumentsCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "arguments";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all available arguments.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return SYSTEM_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new CommandInitException();
        serviceLocator.getCommandService().getCommandArguments().forEach(System.out::println);
        System.out.println();
    }

}
