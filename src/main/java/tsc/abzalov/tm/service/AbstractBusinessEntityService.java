package tsc.abzalov.tm.service;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IBusinessEntityRepository;
import tsc.abzalov.tm.api.IBusinessEntityService;
import tsc.abzalov.tm.enumeration.Status;
import tsc.abzalov.tm.exception.data.EmptyEntityException;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EmptyNameException;
import tsc.abzalov.tm.exception.data.IncorrectIndexException;
import tsc.abzalov.tm.model.BusinessEntity;

import java.time.LocalDateTime;
import java.util.List;

import static org.apache.commons.lang3.ObjectUtils.anyNull;
import static org.apache.commons.lang3.StringUtils.isAnyBlank;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static tsc.abzalov.tm.constant.LiteralConst.DEFAULT_DESCRIPTION;
import static tsc.abzalov.tm.util.InputUtil.isIndexIncorrect;

public abstract class AbstractBusinessEntityService<T extends BusinessEntity> extends AbstractService<T>
        implements IBusinessEntityService<T> {

    @NotNull
    private final IBusinessEntityRepository<T> repository;

    public AbstractBusinessEntityService(@NotNull IBusinessEntityRepository<T> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public int size(@NotNull final String userId) throws Exception {
        if (isBlank(userId)) throw new EmptyIdException();
        return repository.size(userId);
    }

    @Override
    public boolean isEmpty(@NotNull final String userId) throws Exception {
        if (isBlank(userId)) throw new EmptyIdException();
        return repository.isEmpty(userId);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public int indexOf(@NotNull final String userId, @NotNull final T entity) throws Exception {
        if (StringUtils.isEmpty(userId)) throw new EmptyIdException();
        if (entity == null) throw new EmptyEntityException();
        return repository.indexOf(userId, entity);
    }

    @Override
    @NotNull
    public List<T> findAll(@NotNull final String userId) throws Exception {
        if (isBlank(userId)) throw new EmptyIdException();
        return repository.findAll(userId);
    }

    @Override
    @Nullable
    public T findById(@NotNull final String userId, @NotNull final String id) throws Exception {
        if (isAnyBlank(userId, id)) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @Override
    @Nullable
    public T findByIndex(@NotNull final String userId, int index) throws Exception {
        if (isBlank(userId)) throw new EmptyIdException();
        index = index - 1;
        if (isIndexIncorrect(index, size(userId))) throw new IncorrectIndexException(index);
        return repository.findByIndex(userId, index);
    }

    @Override
    @Nullable
    public T findByName(@NotNull final String userId, @NotNull final String name) throws Exception {
        if (isBlank(userId)) throw new EmptyIdException();
        if (isBlank(name)) throw new EmptyNameException();
        return repository.findByName(userId, name);
    }

    @Override
    @Nullable
    public T editById(
            @NotNull final String userId, @NotNull final String id,
            @NotNull final String name, @NotNull String description
    ) throws Exception {
        if (isAnyBlank(userId, name)) throw new EmptyIdException();
        if (isBlank(name)) throw new EmptyNameException();
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;
        return repository.editById(userId, id, name, description);
    }

    @Override
    @Nullable
    public T editByIndex(
            @NotNull final String userId, int index,
            @NotNull final String name, @NotNull String description
    ) throws Exception {
        if (isBlank(userId)) throw new EmptyIdException();
        index = index - 1;
        if (isIndexIncorrect(index, size(userId))) throw new IncorrectIndexException(index);
        if (isBlank(name)) throw new EmptyNameException();
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;
        return repository.editByIndex(userId, index, name, description);
    }

    @Override
    @Nullable
    public T editByName(
            @NotNull final String userId, @NotNull final String name,
            @NotNull String description
    ) throws Exception {
        if (isBlank(userId)) throw new EmptyIdException();
        if (isBlank(name)) throw new EmptyNameException();
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;
        return repository.editByName(userId, name, description);
    }

    @Override
    public void clear(@NotNull final String userId) throws Exception {
        if (isBlank(userId)) throw new EmptyIdException();
        repository.clear(userId);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) throws Exception {
        if (isAnyBlank(userId, id)) throw new EmptyIdException();
        repository.removeById(userId, id);
    }

    @Override
    public void removeByIndex(@NotNull final String userId, int index) throws Exception {
        if (isBlank(userId)) throw new EmptyIdException();
        index = index - 1;
        if (isIndexIncorrect(index, size(userId))) throw new IncorrectIndexException(index);
        repository.removeByIndex(userId, index);
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) throws Exception {
        if (isBlank(userId)) throw new EmptyIdException();
        if (isBlank(name)) throw new EmptyNameException();
        repository.removeByName(userId, name);
    }

    @Override
    @Nullable
    public T startById(@NotNull final String userId, @NotNull final String id) throws Exception {
        if (isAnyBlank(userId, id)) throw new EmptyIdException();
        return repository.startById(userId, id);
    }

    @Override
    @Nullable
    public T endById(@NotNull final String userId, @NotNull final String id) throws Exception {
        if (isAnyBlank(userId, id)) throw new EmptyIdException();
        return repository.endById(userId, id);
    }

    @Override
    @NotNull
    public List<T> sortByName(@NotNull final String userId) throws Exception {
        if (isBlank(userId)) throw new EmptyIdException();

        @NotNull final List<T> entities = repository.findAll(userId);
        entities.sort((first, second) -> {
            @NotNull final String firstName = first.getName();
            @NotNull final String secondName = second.getName();
            return firstName.compareTo(secondName);
        });
        return entities;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    @NotNull
    public List<T> sortByStartDate(@NotNull final String userId) throws Exception {
        if (isBlank(userId)) throw new EmptyIdException();

        @NotNull final List<T> entities = repository.findAll(userId);
        entities.sort((first, second) -> {
            @Nullable final LocalDateTime firstStartDate = first.getStartDate();
            @Nullable final LocalDateTime secondStartDate = second.getStartDate();
            if (anyNull(firstStartDate, secondStartDate)) return 0;
            return firstStartDate.compareTo(secondStartDate);
        });
        return entities;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    @NotNull
    public List<T> sortByEndDate(@NotNull final String userId) throws Exception {
        if (isBlank(userId)) throw new EmptyIdException();

        @NotNull final List<T> entities = repository.findAll(userId);
        entities.sort((first, second) -> {
            @Nullable final LocalDateTime firstEndDate = first.getEndDate();
            @Nullable final LocalDateTime secondEndDate = second.getEndDate();
            if (anyNull(firstEndDate, secondEndDate)) return 0;
            return firstEndDate.compareTo(secondEndDate);
        });
        return entities;
    }

    @Override
    @NotNull
    public List<T> sortByStatus(@NotNull final String userId) throws Exception {
        if (isBlank(userId)) throw new EmptyIdException();

        @NotNull final List<T> entities = repository.findAll(userId);
        entities.sort((first, second) -> {
            @NotNull final Status firstStatus = first.getStatus();
            @NotNull final Status secondStatus = second.getStatus();
            return firstStatus.compareTo(secondStatus);
        });
        return entities;
    }

}
