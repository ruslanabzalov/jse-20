package tsc.abzalov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.BusinessEntity;

import java.util.List;

public interface IBusinessEntityService<T extends BusinessEntity> extends IService<T> {

    int size(@NotNull String userId) throws Exception;

    boolean isEmpty(@NotNull String userId) throws Exception;

    int indexOf(@NotNull String userId, @NotNull T entity) throws Exception;

    @NotNull
    List<T> findAll(@NotNull String userId) throws Exception;

    @Nullable
    T findById(@NotNull String userId, @NotNull String id) throws Exception;

    @Nullable
    T findByIndex(@NotNull String userId, int index) throws Exception;

    @Nullable
    T findByName(@NotNull String userId, @NotNull String name) throws Exception;

    @Nullable
    T editById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description) throws Exception;

    @Nullable
    T editByIndex(@NotNull String userId, int index, @NotNull String name, @NotNull String description) throws Exception;

    @Nullable
    T editByName(@NotNull String userId, @NotNull String name, @NotNull String description) throws Exception;

    void clear(@NotNull String userId) throws Exception;

    void removeById(@NotNull String userId, @NotNull String id) throws Exception;

    void removeByIndex(@NotNull String userId, int index) throws Exception;

    void removeByName(@NotNull String userId, @NotNull String name) throws Exception;

    @Nullable
    T startById(@NotNull String userId, @NotNull String id) throws Exception;

    @Nullable
    T endById(@NotNull String userId, @NotNull String id) throws Exception;

    @NotNull
    List<T> sortByName(@NotNull String userId) throws Exception;

    @NotNull
    List<T> sortByStartDate(@NotNull String userId) throws Exception;

    @NotNull
    List<T> sortByEndDate(@NotNull String userId) throws Exception;

    @NotNull
    List<T> sortByStatus(@NotNull String userId) throws Exception;

}
