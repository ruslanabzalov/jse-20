package tsc.abzalov.tm.exception;

import org.jetbrains.annotations.NotNull;

public abstract class AbstractException extends Exception {

    public AbstractException(@NotNull final String message) {
        super("\n" + message + "\n");
    }

}
